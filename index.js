const { RTMClient, WebClient} = require('@slack/client');
const request = require('request');
const fs = require('fs');

const token = 'xoxb-2151807574-491867460996-TsMMDMG4g8TZNrNmr4Aneo7X';
const rtm = new RTMClient(token);
const web = new WebClient(token);

let team;
let self;

console.log('Welcome CMS member!');

rtm.start();

rtm.on('authenticated', (rtmStartData) => {
  team = rtmStartData.team;
  self = rtmStartData.self;

  console.log(rtmStartData);
  console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}`);
});

rtm.on('connected', function () {
  console.log('Bot is Live');
});

function sendFile(options, fileName, channelId) {
  request(options, (err, res, body) => {
    if (err) {
      return console.log(err);
    }
    const info = JSON.parse(body);
    fs.writeFile(fileName, JSON.stringify(info , null, 4), 'utf8', function(err, data){
      if (err) {
        console.log(err);
      }
      console.log("Successfully Written to File.");

      web.files.upload({fileName,
          file: fs.createReadStream(`./${fileName}`),
          channels: channelId
        })
        .then((res) => {
          console.log('File uploaded: ', res.file.id);
          fs.unlink(fileName, function (err) {
            if (err) throw err;
            // if no error, file has been deleted successfully
            console.log('File deleted!');
          });
        })
        .catch(console.error);

    });

  });
}

function findById(styleId, store, channelId) {
  let storeId = store === 'myntra'? 2297: 4603;
  let fileName = 'temp/' + styleId + "_" + storeId + "_" + "styleResponse_" + (new Date().toTimeString());
  fileName += '.txt';

  const options = {
    url: 'http://catalogservice.myntra.com/myntra-catalog-service/product/' + styleId,
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Basic WVhCcFlXUnRhVzQ2YlRGOnVkSEpoVWpCamEyVjBNVE1oSXc9PQ==',
      'Content-Type': 'application/json',
      'storeid': storeId
    }
  };

  sendFile(options, fileName, channelId);
}

function clearCache(styleId, channelId) {
  let fileName = 'temp/' + styleId + "_clearCache_" + (new Date().toTimeString());
  fileName += '.txt';

  const options = {
    url: 'http://catalogservice.myntra.com/myntra-catalog-service/product/' + styleId + '/clearcache',
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Basic WVhCcFlXUnRhVzQ2YlRGOnVkSEpoVWpCamEyVjBNVE1oSXc9PQ==',
      'Content-Type': 'application/json'
    }
  };

  sendFile(options, fileName, channelId);
}

function searchBySKUId(skuId, store, channelId) {
  let storeId = store === 'myntra'? 2297: 4603;
  let fileName = 'temp/' + skuId + "_" + storeId + "_" + "skuSearch_" + (new Date().toTimeString());
  fileName += '.txt';

  const options = {
    url: 'http://catalogservice.myntra.com/myntra-catalog-service/v2/product/search?q=productOptions.sku.skuId.eq:' + skuId,
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Basic WVhCcFlXUnRhVzQ2YlRGOnVkSEpoVWpCamEyVjBNVE1oSXc9PQ==',
      'Content-Type': 'application/json',
      'storeid': storeId
    }
  };

  console.log(options.url + ' -  ' + storeId );
  sendFile(options, fileName, channelId);
}

rtm.on('message', (message) => {
  let textArray = '';
  let action = '';
  let store = '';
  const cmsChannel = 'G04KS6PFK';
  const channelId = message.channel;
  const app_mention = '<@' + self.id + '>';
  const stores = ['myntra', 'jabong'];
  let supportedActions = "Supported Actions:\n1. For findById - Please type \'" + app_mention + " findById styleId storeName\'\n";
  //supportedActions += "3. For clearCache - Please type \'" + app_mention + " clearCache styleId\'";
  supportedActions += "2. For v2 search(by sku) - Please type \'" + app_mention + " v2search skuId storeName\'";
  let didntUnderstandMessage = "I didn't understand your request. Please type \'" + app_mention + " help\'";

  console.log(message);
  // Skip messages that are from a bot or my own user ID
  if ( (message.subtype && message.subtype === 'bot_message') ||
       (!message.subtype && message.user === rtm.activeUserId) ) {
    return;
  }
  if (channelId === cmsChannel) {
	   console.log('Message Received');
	   return;
  }
  if ((message.text).indexOf(app_mention) === -1) {
    return;
  }

  textArray = message.text.split(' ');
  if (textArray[0] !== app_mention) {
    //rtm.sendMessage("I didn't understand your request. Please type \'@jarvis help\'", channelId);
    return;
  }

  if (textArray.length < 2) {
    rtm.sendMessage(didntUnderstandMessage, channelId);
    return;
  }
  action = (textArray[1]).toLowerCase();
  if (action === 'help') {
    rtm.sendMessage(supportedActions, channelId);
  } else if (action === 'clearcache') {
    if (textArray.length != 3) {
      rtm.sendMessage(didntUnderstandMessage, channelId);
      return;
    }
    if ((/^\d+$/.test(textArray[2]))) {
      clearCache(textArray[2], channelId);
    } else {
      rtm.sendMessage("Please enter the valid style Id.", channelId);
    }
  } else if (action === 'findbyid') {
    if (textArray.length != 4) {
      rtm.sendMessage(didntUnderstandMessage, channelId);
      return;
    }
    store = (textArray[3]).toLowerCase();
    if (!(/^\d+$/.test(textArray[2]))) {
      rtm.sendMessage("Please enter the valid style Id.", channelId);
    } else if (stores.indexOf(store) <= -1) {
      rtm.sendMessage("Please enter the valid store Id", channelId);
    } else {
      findById(textArray[2], store, channelId);
    }
  } else if (action === 'v2search') {
    if (textArray.length != 4) {
      rtm.sendMessage(didntUnderstandMessage, channelId);
      return;
    }
    store = (textArray[3]).toLowerCase();
    if (!(/^\d+$/.test(textArray[2]))) {
      rtm.sendMessage("Please enter the valid SKU Id.", channelId);
    } else if (stores.indexOf(store) <= -1) {
      rtm.sendMessage("Please enter the valid store Id", channelId);
    } else {
      searchBySKUId(textArray[2], store, channelId);
    }
  } else {
    rtm.sendMessage(didntUnderstandMessage, channelId);
  }

  console.log(`(channel:${message.channel}) ${message.user} says: ${message.text}`);
});
